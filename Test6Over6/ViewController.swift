//
//  ViewController.swift
//  Test6Over6
//
//  Created by David Zaituni on 6/15/16.
//  Copyright © 2016 David Zaituni. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var takePicture: UIButton!
    @IBOutlet weak var cameraContainer: UIView!
    
    var motionManager : MotionManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.motionManager = MotionManager()
        self.motionManager.delegate = self
        self.motionManager.startUpdates()
        
        takePicture.hidden = true
    }
    @IBAction func onTakePhotoClick(sender: AnyObject) {
        guard let cameraController = self.childViewControllers.first as? CameraViewController else
        {
            print("Failed to take a photo, controller is missing")
            return
        }
        cameraController.takePhoto()
    }
    
    func rotate(angle : CGFloat) {
        UIView .animateWithDuration(0.5, animations: { () -> Void in
            self.cameraContainer.transform = CGAffineTransformMakeRotation(angle*2)
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func handleTakePictureButton(angle: CGFloat)
    {
        if (angle > -0.05 && angle < 0.05)
        {
            takePicture.hidden = false
        }
        else
        {
            takePicture.hidden = true
        }
    }


}

extension ViewController : MotionManagerDelegate
{
    func didChangeRotationAngle(angle: CGFloat) {
        self.rotate(angle)
        self.handleTakePictureButton(angle)
    }
}
