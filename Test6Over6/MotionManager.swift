//
//  MotionManager.swift
//  Test6Over6
//
//  Created by David Zaituni on 6/15/16.
//  Copyright © 2016 David Zaituni. All rights reserved.
//

import UIKit
import CoreMotion

protocol MotionManagerDelegate {
    func didChangeRotationAngle(angle : CGFloat)
}

class MotionManager: NSObject {
    
    private var motionManager: CMMotionManager!
    private let logQueue: NSOperationQueue!
    
    var isStopped : Bool = false
    var delegate : MotionManagerDelegate!
    
    override init() {
        self.logQueue = NSOperationQueue()
        
    }
    
    func startUpdates() {
        self.isStopped = false
        self.motionManager = CMMotionManager()
        self.motionManager.deviceMotionUpdateInterval = 5.0 / 60.0;
        self.startAccelerometerUpdates()
    }
    
    private func startAccelerometerUpdates () {
        if motionManager.accelerometerAvailable {
            motionManager.accelerometerUpdateInterval = 0.1
            motionManager.startAccelerometerUpdatesToQueue(NSOperationQueue.mainQueue()) {
                [weak self] (data: CMAccelerometerData?, error: NSError?) in
                if let isStop = self?.isStopped {
                    if isStop {
                        return
                    }
                }
                if let acceleration = data?.acceleration {
                    let rotation = atan2(acceleration.x, acceleration.y) - M_PI
                    if let delegate = self?.delegate {
                        delegate.didChangeRotationAngle(CGFloat(rotation))
                    }
                }
            }
        }
    }
    
    
    func stopUpdates() {
        isStopped = true
        motionManager = nil
        motionManager.stopDeviceMotionUpdates()
        motionManager.stopGyroUpdates()
    }
    
    
//    private func getAccelerometerData() {
//        if let accelerometerData = motionManager.accelerometerData {
//            let gravity = CGVector(dx: accelerometerData.acceleration.y * -50, dy: accelerometerData.acceleration.x * 50)
//        }
//    }
    
}
